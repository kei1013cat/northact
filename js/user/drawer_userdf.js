$(document).ready(function() {
    $('.drawer').drawer();

    // ドロワーメニューが開いたとき
    $('.drawer').on('drawer.opened', function(){
        $('#menu-text').text('CLOSE');
    });
    // ドロワーメニューが閉じたとき
    $('.drawer').on('drawer.closed', function(){
        $('#menu-text').text('MENU');
    });
});