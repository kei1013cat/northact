jQuery(function($) {
	var headNav = $("header .pcmenu .header_bottom");
    var headNavDam = $("header .pcmenu .header_bottom_dammy");
	//scrollだけだと読み込み時困るのでloadも追加
	$(window).on('load scroll', function () {
		//現在の位置が500px以上かつ、クラスfixedが付与されていない時
		if($(this).scrollTop() > 200 && headNav.hasClass('fixed') == false) {
			//headerの高さ分上に設定
			headNav.css({"top": '-100px'});
			//クラスfixedを付与
			headNav.addClass('fixed');
            headNavDam.css('display', 'block');
			//位置を0に設定し、アニメーションのスピードを指定
			headNav.animate({"top": 0},600);
		}

		//現在の位置が300px以下かつ、クラスfixedが付与されている時にfixedを外す
		else if($(this).scrollTop() < 151 && headNav.hasClass('fixed') == true){
			headNav.removeClass('fixed');
			headNavDam.css('display', 'none');
		}
	});
});