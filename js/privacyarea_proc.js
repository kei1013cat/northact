jQuery(function($){
  $(".privacy label").append("<div class='privacy-em'>必須</em>");

  if($("input[name='個人情報保護方針[data][]']").prop("checked")) {
    $("input:checkbox[name='個人情報保護方針[data][]']").prop({'disabled':false});
    $("#page_contact section.form .privacy label").removeClass('disable');
  } else {
    $("input:checkbox[name='個人情報保護方針[data][]']").prop({'disabled':true});
    $(".form .privacy label").addClass('disable');
  }

  $('.privacy_area').on('scroll', function(){
    var innerHeight = $('.privacy_area')[0].scrollHeight, //内側の要素の高さ
        outerHeight = $('.privacy_area').scrollTop(), //外側の要素の高さ
        outerBottom = innerHeight - outerHeight; //内側の要素の高さ - 外側の要素の高さ

    if(outerBottom <= 320) {
    	$("input:checkbox[name='個人情報保護方針[data][]']").prop({'disabled':false});
		$("#page_contact section.form .privacy label").removeClass('disable');
    }

  });


});