<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width">
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <title>
        <?php if(is_page() && $post->post_name != 'home'){ wp_title('');echo '  '; }
 bloginfo('name'); ?>
    </title>
    <meta name="keyword" content="" />
    <link rel="stylesheet" href="<?php bloginfo( 'stylesheet_url' ); ?>" />

    <!-- favicon -->
    <link rel="apple-touch-icon" sizes="180x180" href="<?php bloginfo('template_url'); ?>/images/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php bloginfo('template_url'); ?>/images/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php bloginfo('template_url'); ?>/images/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?php bloginfo('template_url'); ?>/images/favicon/site.webmanifest">
    <link rel="mask-icon" href="<?php bloginfo('template_url'); ?>/images/favicon/safari-pinned-tab.svg" color="#ffa243">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">


    <?php if(is_pc()):?>
    <!--[if lt IE 9]>
    <script src="<?php bloginfo('template_url'); ?>/js/html5.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/js/respond.js"></script>
    <![endif]-->
    <?php endif; ?>
    <?php //以下user設定 ?>
    <script src="<?php bloginfo('template_url'); ?>/js/jquery-3.3.1.min.js"></script>

    <!-- scrollreveal -->
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/scrollreveal/scrollreveal.js"></script>
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/scrollreveal/scrollreveal.thema.js"></script>

    <!-- accordion -->
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/accordion.js"></script>
    <!-- smoothScroll -->
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/smoothScroll.js"></script>
    <!-- pulldown -->
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/pulldown.js"></script>

    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/lording.js"></script>

    <!-- matchHeight -->
    <script src="<?php bloginfo('template_url'); ?>/js/match-height/jquery.matchHeight.js"></script>
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/user/matchHeight_userdf.js"></script>

    <!-- rwdImageMaps -->
    <!--
<script src="<?php bloginfo('template_url'); ?>/js/rwdImageMaps/jquery.rwdImageMaps.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/user/rwdImageMaps_userdf.js"></script>
-->

    <!-- drawer -->
    <script src="<?php bloginfo('template_url'); ?>/js/iscroll.min.js"></script>
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/js/drawer/dist/css/drawer.min.css">
    <script src="<?php bloginfo('template_url'); ?>/js/drawer/dist/js/drawer.min.js"></script>
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/user/drawer_userdf.js"></script>


    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/print.css" media="print">

    <script type="text/javascript">
        jQuery(function($) {
            $('#mw_wp_form_mw-wp-form-61 select option[value=""]')
                .html('選択してください');
        });

    </script>

    <script>
        $(function() {
            <?php if ($_GET["entry_type"] != NULL): ?>
            $('#entry_type').val("<?php echo $_GET['entry_type']; ?>");
            <?php endif; ?>
        });

    </script>


    <!-- スライダー(vegas)-->
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/js/vegas/vegas.css">
    <script src="<?php bloginfo('template_url'); ?>/js/vegas/vegas.js"></script>
    <script>
        $(function() {
            $('#mainvisual').vegas({
                slides: [{
                        src: 'wp-content/themes/northact/images/slider1.jpg?v=20190319',
                        msg: '<h2><img src="<?php bloginfo('template_url'); ?>/images/maintext1.svg" alt="" /></h2>'
                    },
                    {
                        src: 'wp-content/themes/northact/images/slider2.jpg?v=20190419',
                        msg: '<h2><img src="<?php bloginfo('template_url'); ?>/images/maintext2.svg?v=20190419" alt="" /></h2>'
                    },
                    {
                        src: 'wp-content/themes/northact/images/slider3.jpg?v=20190319',
                        msg: '<h2><img src="<?php bloginfo('template_url'); ?>/images/maintext3.svg" alt="" /></h2>'
                    },
                    {
                        src: 'wp-content/themes/northact/images/slider4.jpg?v=20190319',
                        msg: '<h2><img src="<?php bloginfo('template_url'); ?>/images/maintext4.svg" alt="" /></h2>'
                    },
                ],
                overlay: false,
                animation: 'fead2',
                transitionDuration: 1500,
                delay: 7000,
                animationDuration: 18000,
                timer: false
            });

        });

    </script>

    <script>
        jQuery(function() {
            $('.mouseover').each(function() {
                var src_off = $(this).find('img').attr('src');
                var src_on = src_off.replace('_off', '_on');
                $('<img />').attr('src', src_on);
                $(this).hover(function() {
                    $(this).find('img').attr('src', src_on);
                }, function() {
                    $(this).find('img').attr('src', src_off);
                });
            });
        });

    </script>

    <!-- etc -->
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/product_apply.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/js/ajaxzip3.js" charset="UTF-8"></script>
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/user/ajaxzip3_userdf.js"></script>
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/privacyarea_proc.js"></script>

    <?php if(is_front_page()): ?>

    <!-- slick -->
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/js/slick/slick/slick.css" type="text/css">
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/js/slick/slick/slick-theme.css" type="text/css">
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/slick/slick/slick.min.js"></script>
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/user/slick_userdf.js"></script>

    <?php endif; ?>


    <?php wp_head(); ?>
</head>
<?php
  $body_id = "";
  $body_class = "";
  if ( is_front_page() ) {
    $body_id = ' id="page_index"';
  }else if ( is_page() ) {
    if($post -> post_parent == 0 ){
        $body_id = ' id="page_'.$post->post_name.'"';
    }else{
        $ancestors =  $post-> ancestors;
        foreach($ancestors as $ancestor){
            $body_id = ' id="page_'.get_post($ancestor)->post_name.'"';
            break;
        }
    }
    $body_class = ' subpage';
    if( $post->post_parent){
        $body_class = ' '.$post->post_name;
    }
  }else if (  is_single() ) {
    $body_id = ' id="page_single"';
   $body_class = " subpage ".get_post_type( $post );
  }else if ( is_archive() ) {
    $body_id = ' id="page_archive"';
    $body_class = " subpage ".get_post_type( $post );
  }else if ( is_404() ) {
    $body_id = ' id="page_404"';
    $body_class = ' subpage';
  }
?>

<?php
    if ($_GET["area"] != NULL) {
        $area = $_GET["area"];
    }
?>

<body<?php echo $body_id; ?> class="drawer drawer--top<?php echo $body_class; ?>">
    <div id="loading_wrap"></div>
    <div id="outer">
        <header>
            <div class="pcmenu cf">
                <div class="header_top cf">
                    <div class="wrapper">
                        <h1>
                            <a href="<?php bloginfo('url'); ?>/"><img src="<?php bloginfo('template_url'); ?>/images/header_logo.svg" alt="" /></a>
                            <span class="text">精神科特化型訪問看護ステーション</span>
                        </h1>
                        <div class="header_contact cf">
                            <ol class="cf">
                                <li><a href="<?php bloginfo('url'); ?>/blog/">スタッフブログ</a></li>
                                <li><a href="<?php bloginfo('url'); ?>/access/">アクセス</a></li>
                            </ol>
                            <div class="contact"><a href="<?php bloginfo('url'); ?>/contact/">お問い合わせ</a></div>
                        </div>
                        <!-- header_contact -->
                    </div>
                    <!-- contact -->
                </div>
                <!-- header_top -->
                <div class="header_bottom cf">
                    <div class="wrapper">
                        <nav>
                            <ul class="cf">
                                <li class="home"><a class="mouseover" href="<?php bloginfo('url'); ?>/"><img src="<?php bloginfo('template_url'); ?>/images/header_home_off.svg" alt="ホーム" /></a></li>
                                <li class="visitingcare"><a href="<?php bloginfo('url'); ?>/visitingcare/">精神科訪問看護</a></li>
                                <li class="group"><a href="<?php bloginfo('url'); ?>/group/">グループホーム</a></li>
                                <!--
                                <li class="staff"><a href="<?php bloginfo('url'); ?>/staff/">ピアスタッフ</a>
                                </li>
                                <li class="team"><a href="<?php bloginfo('url'); ?>/team/">チーム紹介</a>
                                </li>
                                <li class="medical"><a href="<?php bloginfo('url'); ?>/partner/">提携医療・福祉機関</a>
                                </li>-->
                                <li class="company"><a href="<?php bloginfo('url'); ?>/company/">会社概要</a></li>
                                <li class="recruit"><a href="<?php bloginfo('url'); ?>/recruit/">採用情報</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                    <!-- wrapper -->
                </div><!-- header_bottom -->
                <div class="header_bottom_dammy"></div>
            </div>
            <!-- pcmenu -->

            <div class="spmenu drawermenu" role="banner" id="top">
                <h1 class="cf"><a href="<?php bloginfo('url'); ?>/"><img src="<?php bloginfo('template_url'); ?>/images/header_logo.svg" alt="" /><span class="text">精神科特化型<br>訪問看護ステーション</span></a></h1>
                <button type="button" class="drawer-toggle drawer-hamburger">
                    <span class="sr-only">toggle navigation</span>
                    <span class="drawer-hamburger-icon"></span>
                    <div id="menu-text" class="text">MENU</div>
                </button>
                <nav class="drawer-nav" role="navigation">
                    <div class="nav-top">
                        <ul class="cf">
                            <li><a href="<?php bloginfo('url'); ?>/blog/"><img src="<?php bloginfo('template_url'); ?>/images/header_blog_icon.svg" alt="" /><span class="text">スタッフブログ</span></a></li>
                            <li><a target="_blank" href="https://www.google.com/maps?ll=43.11441,141.332838&z=19&t=m&hl=ja&gl=JP&mapclient=embed&q=%E3%80%92001-0908+%E5%8C%97%E6%B5%B7%E9%81%93%E6%9C%AD%E5%B9%8C%E5%B8%82%E5%8C%97%E5%8C%BA%E6%96%B0%E7%90%B4%E4%BC%BC%EF%BC%98%E6%9D%A1%EF%BC%93%E4%B8%81%E7%9B%AE%EF%BC%93%E2%88%92%EF%BC%91%EF%BC%94"><img src="<?php bloginfo('template_url'); ?>/images/header_map_icon.svg" alt="" /><span class="text">Google Maps</span></a></li>
                            <li><a href="<?php bloginfo('url'); ?>/contact/"><img src="<?php bloginfo('template_url'); ?>/images/header_mail_icon.svg" alt="" /><span class="text">お問い合わせ</span></a></li>
                        </ul>
                    </div>

                    <div class="inner">
                        <h3 class="sp-title">MENU</h3>
                        <ul class="drawer-menu cf">
                            <li class="color"><a class="drawer-menu-item" href="<?php bloginfo('url'); ?>/">ホーム</a></li>
                            <li class="color"><a class="drawer-menu-item" href="<?php bloginfo('url'); ?>/visitingcare/">精神科訪問看護</a></li>
                            <li class="color"><a class="drawer-menu-item" href="<?php bloginfo('url'); ?>/group/">グループホーム</a></li>
                            <!--
                            <li class="color"><a class="drawer-menu-item" href="<?php bloginfo('url'); ?>/staff/">ピアスタッフ</a></li>
                            <li class="color"><a class="drawer-menu-item" href="<?php bloginfo('url'); ?>/team/">チーム紹介</a></li>
                            <li class="color"><a class="drawer-menu-item" href="<?php bloginfo('url'); ?>/partner/">提携医療・福祉機関</a></li>-->
                            <li class="color"><a class="drawer-menu-item" href="<?php bloginfo('url'); ?>/access/">アクセス</a></li>
                            <li class="color"><a class="drawer-menu-item" href="<?php bloginfo('url'); ?>/company/">会社概要</a></li>
                            <li class="color"><a class="drawer-menu-item" href="<?php bloginfo('url'); ?>/recruit/">採用情報</a></li>
                            <li class="color"><a class="drawer-menu-item" href="<?php bloginfo('url'); ?>/blog/">スタッフブログ</a></li>
                            <li class="color"><a class="drawer-menu-item" href="<?php bloginfo('url'); ?>/contact/">お問い合わせ</a></li>
                        </ul>
                        <h3 class="sp-title">NEWS</h3>
                        <ul class="drawer-menu cf">
                            <li class="color"><a class="drawer-menu-item" href="<?php bloginfo('url'); ?>/newslist/">お知らせ</a></li>
                            <li class="color"><a class="drawer-menu-item" href="<?php bloginfo('url'); ?>/newsletter/">ニュースレター</a></li>
                        </ul>
                    </div>
                    <!-- inner -->
                </nav>
            </div>
            <!-- spmenu -->

        </header>
        <main role="main">
