<div id="page_archive">
    <?php get_header(); ?>


    <?php
$paged = (int) get_query_var('paged');
$wp_query = new WP_Query();
$param = array(
'post_status' => 'publish',
'paged' => $paged,
'orderby' => 'date', //ID順に並び替え
'order' => 'DESC'
);
$wp_query->query($param);
?>
    <div id="contents">
        <?php include (TEMPLATEPATH . '/part-title.php'); ?>
        <section class="blog bg_pink">
            <div class="wrapper cf">

                <h2 class="headline2 pt_l pb enter-top"><span class="line">お知らせ記事一覧</span></h2>
                <?php if ( have_posts() ) :?>
                <section class="news">
                    <div class="outer">
                        <?php while($wp_query->have_posts()) :?>
                        <?php $wp_query->the_post(); ?>

                        <dl class="inner cf">
                            <dt>
                                <span class="date"><?php the_time('Y.m.d'); ?></span>
                                <?php
                                $category = get_the_category();
                                if(!empty($category)):?>
                                <span class="cat orange"><?php echo $category[0]->cat_name; ?></span>
                                <?php endif; ?>
                            </dt>
                            <dd>
                                <a href="<?php the_permalink() ?>" rel="bookmark" title="<?php printf(__('Permanent Link to %s', 'kubrick'), the_title_attribute('echo=0')); ?>">
                                    <div class="blog_list cf">
                                        <div class="title"><span class="text"><?php echo $post->post_title; ?></span></div>
                                    </div>
                                </a>
                            </dd>
                        </dl>

                        <?php endwhile; ?>
                    </div>

                    <div id="pagination" class="pagination">
                        <?php global $wp_rewrite;
                    $paginate_base = get_pagenum_link(1);
                    if(strpos($paginate_base, '?') || ! $wp_rewrite->using_permalinks()){
                        $paginate_format = '';
                        $paginate_base = add_query_arg('paged','%#%');
                    }
                    else{
                        $paginate_format = (substr($paginate_base,-1,1) == '/' ? '' : '/') .
                        user_trailingslashit('page/%#%/','paged');;
                        $paginate_base .= '%_%';
                    }
                    echo paginate_links(array(
                        'base' => $paginate_base,
                        'format' => $paginate_format,
                        'total' => $wp_query->max_num_pages,
                        'mid_size' => 4,
                        'current' => ($paged ? $paged : 1),
                        'prev_text' => '« 前へ',
                        'next_text' => '次へ »',
                    )); ?>
                    </div><!-- pagination -->
                </section>
                <?php else : ?>
                記事が見つかりません。
                <?php endif; ?>
                <?php wp_reset_query(); ?>

            </div>
            <!-- wrapper -->
        </section>
    </div>
    <!-- contents -->
    <?php get_footer(); ?>
</div>
