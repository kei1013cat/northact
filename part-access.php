<section class="access">
    <section class="bg_pink">
        <div class="wrapper pb_l wrap-sm">
            <h2>株式会社 north-ACT（ノースアクト）</h2>

            <dl class="cf">
                <dt>住所</dt>
                <dd class="pt_s pb_s">札幌市北区新琴似8条3丁目3番14号<br>
                    <span class="small">札幌駅より車で15分<br>
                    地下鉄南北線麻生駅より徒歩10分</span></dd>
            </dl>
            <dl class="cf pb_l">
                <dt>電話・FAX</dt>
                <dd class="pt pb">電話：011-214-1774<br>FAX ：011-214-0460</dd>
            </dl>
        </div>
        <!-- wrapper -->
    </section>
    
    <div class="map">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d182.03398407962072!2d141.33288117659063!3d43.11410242403557!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f43.1!3m3!1m2!1s0x5f0b28afdd9b2389%3A0xf11a3103d31dcf07!2z44CSMDAxLTA5MDgg5YyX5rW36YGT5pyt5bmM5biC5YyX5Yy65paw55C05Ly877yY5p2h77yT5LiB55uu77yT4oiS77yR77yU!5e0!3m2!1sja!2sjp!4v1553076324293" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>
    <div class="wrapper_small">
    <ul class="cf grid_col4 tab4 sp2 pb pt enter-bottom">
        <li class="col"><img src="<?php bloginfo('template_url'); ?>/images/access_photo1.jpg" ></li>
        <li class="col"><img src="<?php bloginfo('template_url'); ?>/images/access_photo2.jpg" ></li>
        <li class="col"><img src="<?php bloginfo('template_url'); ?>/images/access_photo3.jpg" ></li>
        <li class="col"><img src="<?php bloginfo('template_url'); ?>/images/access_photo4.jpg" ></li>
    </ul>
    </div>
</section>




