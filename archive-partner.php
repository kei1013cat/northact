<?php get_header(); ?>

<div id="contents_wrap">
    <?php get_template_part('part-title'); ?>
    <?php get_template_part('part-pan'); ?>
    <div id="contents">

        <section id="page_post">
            <section class="partner">

                <?php
                $wp_query = new WP_Query();
                $param = array(
                'post_status' => 'publish',
                'paged' => $paged,
                'post_type' => 'partner',
                'order' => 'DESC'
                );
                $wp_query->query($param);?>

                <section class="link bg_pink pb_l pt_l">
                    <div class="wrapper wrap-sm pb_l">

                        <?php if(get_field('施設情報',398)): ?>
                        <h3 class="headline3">病院</h3>
                        <?php while(the_repeater_field('施設情報',398)): ?>
                        <dl class="cf">
                            <dt><?php the_sub_field('施設名'); ?></dt>
                            <dd><a href="<?php the_sub_field('リンク先'); ?>" target="_blank"><?php the_sub_field('リンク先'); ?></a>
                            <dd>
                        </dl>
                        <?php endwhile; ?>
                        <?php endif; ?>


                        <?php if(get_field('施設情報',400)): ?>
                        <h3 class="headline3">クリニック</h3>
                        <?php while(the_repeater_field('施設情報',400)): ?>
                        <dl class="cf">
                            <dt><?php the_sub_field('施設名'); ?></dt>
                            <dd><a href="<?php the_sub_field('リンク先'); ?>" target="_blank"><?php the_sub_field('リンク先'); ?></a>
                            <dd>
                        </dl>
                        <?php endwhile; ?>
                        <?php endif; ?>


                        <?php if(get_field('施設情報',402)): ?>
                        <h3 class="headline3">作業所</h3>
                        <?php while(the_repeater_field('施設情報',402)): ?>
                        <dl class="cf">
                            <dt><?php the_sub_field('施設名'); ?></dt>
                            <dd><a href="<?php the_sub_field('リンク先'); ?>" target="_blank"><?php the_sub_field('リンク先'); ?></a>
                            <dd>
                        </dl>
                        <?php endwhile; ?>
                        <?php endif; ?>


                        <?php if(get_field('施設情報',404)): ?>
                        <h3 class="headline3">相談支援事業所</h3>
                        <?php while(the_repeater_field('施設情報',404)): ?>
                        <dl class="cf">
                            <dt><?php the_sub_field('施設名'); ?></dt>
                            <dd><a href="<?php the_sub_field('リンク先'); ?>" target="_blank"><?php the_sub_field('リンク先'); ?></a>
                            <dd>
                        </dl>
                        <?php endwhile; ?>
                        <?php endif; ?>

                        <?php if(get_field('施設情報',406)): ?>
                        <h3 class="headline3">その他</h3>
                        <?php while(the_repeater_field('施設情報',406)): ?>
                        <dl class="cf">
                            <dt><?php the_sub_field('施設名'); ?></dt>
                            <dd><a href="<?php the_sub_field('リンク先'); ?>" target="_blank"><?php the_sub_field('リンク先'); ?></a>
                            <dd>
                        </dl>
                        <?php endwhile; ?>
                        <?php endif; ?>

                        <?php wp_reset_query(); ?>

                    </div>
                    <!-- wrapper -->
                </section>
                <!-- list -->

            </section>
            <!-- partner -->
        </section>
    </div>
    <!-- contents -->

</div>
<?php get_footer(); ?>
