<section class="visitingcare">
    <section class="act bg_pink pb_l">
        <div class="wrapper wrap-sm">
            <h2 class="headline2 pt_l enter-top"><span class="line">ACTメンバー</span></h2>
            
            <ul class="pb">
                <li>・ACTスタンダーズより展開、365日・24時間</li>
                <li>・保健師・看護師・作業療法士・精神保健福祉士・オフィススタッフ</li>
            </ul>
            <dl class="enter-bottom">
                <dt><h3>ACTとは？</h3></dt>
                <dd>
                    <p class="pb_s">重い精神障害を抱えた人が住み慣れた場所で安心して暮らしていけるように、様々な職種の専門家から構成されるメンバーが支援を提供するプログラムです。<br>
                    英語の’Assertive Community Treatment'　という言葉を略してACTと呼ばれます。私たちは日本語で<span class="red">『包括地域生活支援プログラム』</span>とも呼んでいます。</p>
                    <p class="pb_s">簡単に言うと、お伺い型の保健・医療・福祉サービスのことを言います。</p>
                    <p>north-ACTスタンダーズ（法律）があり、これに該当する利用者様をエントリーします。</p>
                </dd>
            </dl>
        </div>
        <!-- wrapper -->
    </section>
    <!-- act -->
    
    <section class="member bg_green pb">
        <div class="wrapper wrap-sm">
            <h2 class="headline2 pt_l enter-top"><span class="line">精神科訪問看護（訪看）メンバー</span></h2>
            <ul class="pb">
                <li>・ACTの要素、365日・24時間、ACTメンバーのサポート</li>
                <li>・保健師・看護師</li>
            </ul>
            <div class="box enter-bottom">
                ACTプログラムの理念・要素を取り入れた訪問看護を展開します。<br>
                身体的サポート・精神的サポート・社会的サポート・福祉的サポート・家族のサポート・
                地域へのサポートを実施します。
            </div>
            <!-- box -->
            <ul class="list pt pb enter-bottom">
                <li>ストレングス、リカバリーに基づいた訪問看護の展開</li>
                <li>精神障がいが重たい場合は、ACTへエントリーできる</li>
                <li>準ACTの疾患を範囲までとする</li>
                <li>精神科訪問看護が受けることが出来れば、病名は問わない。</li>
                <li>ITT（社内・社外）の実施</li>
                <li>住環境の整備</li>
                <li>医療・福祉の同時展開</li>
                <li>24時間、365日の訪問看護ステーション（オンコール体制）</li>
            </ul>
            
            <dl class="cf col1 pt_s">
                <dt class="fead1">1.身体的援助</dt>
                <dd class="enter-left-slow1">副作用有無</dd>
            </dl>
            <dl class="cf col2">
                <dt class="fead2">2.精神的援助</dt>
                <dd class="enter-left-slow1">服薬状況・時間</dd>
            </dl>
            <dl class="cf col3">
                <dt class="fead3">３.社会的援助</dt>
                <dd class="enter-left-slow1">地域との接点を作る</dd>
            </dl>
            <dl class="cf col4">
                <dt class="fead4">4.住環境整備</dt>
                <dd class="enter-left-slow1">住んでいる環境を整える</dd>
            </dl>
            
            <div class="sp member_photo enter-bottom"><img src="<?php bloginfo('template_url'); ?>/images/member_photo_sp.svg" ></div>

        </div>
        <!-- wrapper -->
    </section>
    <!-- member -->
</section>
<!-- visitingcare -->