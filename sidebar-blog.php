<div id="sidebar" class="pt_l pb_l">
    <aside>
        <div class="cat_list mb">
            <h4 class="head-title">カテゴリー</h4>
            <ul class="pt">
                <?php
            wp_list_categories(array(
                'title_li' =>'',
                'show_count' => 1,
                'taxonomy' => 'blog-cat'
            )); ?>
            </ul>
        </div>
        <!--category -->

        <!--
        <div class="archive_list">
            <h4 class="head-title">過去記事</h4>

            <ul class="pt">
                <div class="select-wrap">
                    <select name="archive-dropdown" onChange='document.location.href=this.options[this.selectedIndex].value;'>
                        <option value=""><?php echo attribute_escape(__('アーカイブを選択')); ?></option>
                        <?php wp_get_archives('type=monthly&format=option&show_post_count=1&post_type=blog'); ?>
                    </select>
                </div>
            </ul>
        </div>-->
        <!-- archive_list -->

        <?php
        $wp_query = new WP_Query();
        $param = array(
        'posts_per_page' => '5',
        'post_status' => 'publish',
        'orderby' => 'date',
        'post_type' => 'blog',
        'order' => 'DESC'
        );
        $wp_query->query($param);?>
        <?php if($wp_query->have_posts()):?>
        <div class="latest_list pt mb">
            <h4 class="head-title">直近の記事</h4>
            <ul class="mt">
                <?php while($wp_query->have_posts()) :?>
                <?php $wp_query->the_post(); ?>
                <li class="bg_gray mb_s">


                    <a href="<?php the_permalink() ?>" rel="bookmark" title="<?php printf(__('Permanent Link to %s', 'kubrick'), the_title_attribute('echo=0')); ?>">
                        <time class="date"><?php the_time('Y.m.d'); ?></time>
                        <?php
                                $category = get_the_terms($post_id, 'blog-cat');
                                if(!empty($category)):?>
                        <span class="cat green"><?php echo $category[0]->name; ?></span>
                        <?php endif; ?>
                        <h3><?php echo $post->post_title; ?></h3>
                    </a>


                </li>
                <?php endwhile; ?>
            </ul>
        </div>
        <!--latest_list -->
        <?php endif; ?>

    </aside>
</div><!-- sidebar -->
