<section class="company pb_l">
    <section class="top bg_pink pt_l">
        <div class="wrapper wrap-sm">
            <table class="style01 mb mt_s">
                <tbody>
                    <tr>
                        <th>会社名</th>
                        <td>株式会社 north-ACT （ノースアクト)</td>
                    </tr>
                    <tr>
                        <th>事業所番号</th>
                        <td>0160290565</td>
                    </tr>
                    <tr>
                        <th>事業内容</th>
                        <td>精神科訪問看護、グループホーム運営</td>
                    </tr>
                    <tr>
                        <th>拠点</th>
                        <td>
                            <h3 class="headline3">ACTメンバー</h3>
                            <p>
                                保健師<br>
                                看護師<br>
                                作業療法士<br>
                                精神保健福祉士<br>
                                オフィススタッフ
                            </p>
                            <h3 class="headline3 mt">精神科訪問看護メンバー</h3>
                            <p>保健師・看護師</p>
                        </td>
                    </tr>
                    <tr>
                        <th>住所</th>
                        <td>札幌市北区新琴似8条3丁目3番14号<br>
                            札幌駅より車で15分<br>
                            地下鉄南北線麻生駅より徒歩10分</td>
                    </tr>
                    <tr>
                        <th>電話・FAX</th>
                        <td>電話：011-214-1774<br>FAX：011-214-0460</td>
                    </tr>
                </tbody>
            </table>

            <ul class="grid_col3 cf enter-bottom">
                <li class="col"><img src="<?php bloginfo('template_url'); ?>/images/company_photo1.jpg"></li>
                <li class="col"><img src="<?php bloginfo('template_url'); ?>/images/company_photo2.jpg"></li>
                <li class="col"><img src="<?php bloginfo('template_url'); ?>/images/company_photo3.jpg"></li>
            </ul>

        </div>
        <!-- wrapper -->
    </section>
    <!-- top -->
    <section class="business">
        <div class="wrapper wrap-sm">
            <h2 class="headline2 pt_l enter-top pb_s"><span class="line">事業内容</span></h2>
            <div class="outer cf pb_l">
                <div class="photo">
                    <img src="<?php bloginfo('template_url'); ?>/images/company_business_photo1.jpg">
                </div>
                <!-- photo -->
                <div class="text">
                    <h3 class="headline3">精神科訪問看護</h3>
                    <p>一人ひとりと向き合い、重い精神障害を抱えた人でも
                        住み慣れた家で安心した生活ができるようサポートを行っております。</p>
                    <p class="linkbtn pt"><a href="<?php bloginfo('url'); ?>/visitingcare/">詳しく見る</a></p>
                </div>
                <!-- text -->
            </div>
            <!-- outer -->
            <div class="outer cf pb_l">
                <div class="photo">
                    <img src="<?php bloginfo('template_url'); ?>/images/company_business_photo2.jpg">
                </div>
                <!-- photo -->
                <div class="text">
                    <h3 class="headline3">グループホーム</h3>
                    <p>グループホーム「フィット麻生・パステル麻生」では精神障がい者への共同生活を援助しております。</p>
                    <p class="linkbtn pt"><a href="<?php bloginfo('url'); ?>/group/">詳しく見る</a></p>
                </div>
                <!-- text -->
            </div>
            <!-- outer -->
            <div class="outer cf">
                <div class="photo">
                    <img src="<?php bloginfo('template_url'); ?>/images/company_business_photo3.jpg">
                </div>
                <!-- photo -->
                <div class="text">
                    <h3 class="headline3">就労支援</h3>
                    <p>農業・水耕栽培・特殊清掃で就労支援をしております。</p>
                    <ul class="list">
                        <li>畑の仕事</li>
                        <li>水耕の仕事</li>
                        <li>清掃の仕事</li>
                        <li>消臭の仕事</li>
                        <li>事務の仕事</li>
                    </ul>

                </div>
                <!-- text -->
            </div>
            <!-- outer -->
        </div>
        <!-- wrapper  -->
    </section>
    <!-- business -->

    <section class="recruit pt_l pb">
        <div class="wrapper">
            <h3>ノースアクトで一緒に働きませんか？</h3>
            <p class="linkbtn1 pt_s"><a href="<?php bloginfo('url'); ?>/recruit/">採用情報へ</a></p>
        </div>
    </section>
    <!-- recruit -->
</section>
