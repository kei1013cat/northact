<?php get_header(); ?>

<div id="contents_wrap">
    <?php get_template_part('part-title'); ?>
    <?php get_template_part('part-pan'); ?>
    <div id="contents">
        <section id="page_applicant">
            <section class="applicant bg_pink pt_s">
                <div class="wrapper wrap-sm">
                    <h2 class="enter-top"><img class="pt pb_s img_center" src="<?php bloginfo('template_url'); ?>/images/headline_recruit.svg" alt="募集要項"></h2>
                    <?php if( !get_field('募集対象外の時期')): ?>
                    <p class="pt_s pb_s"><?php echo get_field('タイトルメッセージ'); ?></p>

                    <table class="style02 mb mt_s">
                        <tbody>
                            <?php if(get_field('募集職種')): ?><tr>
                                <th>募集職種</th>
                                <td><?php echo get_field('募集職種'); ?></td>
                            </tr><?php endif; ?>
                            <?php if(get_field('雇用形態')): ?><tr>
                                <th>雇用形態</th>
                                <td><?php echo get_field('雇用形態'); ?></td>
                            </tr><?php endif; ?>
                            <?php if(get_field('業務内容')): ?><tr>
                                <th>業務内容</th>
                                <td><?php echo get_field('業務内容'); ?></td>
                            </tr><?php endif; ?>
                            <?php if(get_field('施設形態')): ?><tr>
                                <th>施設形態</th>
                                <td><?php echo get_field('施設形態'); ?></td>
                            </tr><?php endif; ?>
                            <?php if(get_field('給与')): ?><tr>
                                <th>給与</th>
                                <td><?php echo get_field('給与'); ?></td>
                            </tr><?php endif; ?>
                            <?php if(get_field('資格')): ?><tr>
                                <th>資格</th>
                                <td><?php echo get_field('資格'); ?></td>
                            </tr><?php endif; ?>
                            <?php if(get_field('勤務地')): ?><tr>
                                <th>勤務地</th>
                                <td><?php echo get_field('勤務地'); ?></td>
                            </tr><?php endif; ?>
                            <?php if(get_field('勤務時間')): ?><tr>
                                <th>勤務時間</th>
                                <td><?php echo get_field('勤務時間'); ?></td>
                            </tr><?php endif; ?>
                            <?php if(get_field('最寄り交通機関')): ?><tr>
                                <th>最寄り交通機関</th>
                                <td><?php echo get_field('最寄り交通機関'); ?></td>
                            </tr><?php endif; ?>
                            <?php if(get_field('休暇・休日')): ?><tr>
                                <th>休暇・休日</th>
                                <td><?php echo get_field('休暇・休日'); ?></td>
                            </tr><?php endif; ?>
                            <?php if(get_field('備考')): ?><tr>
                                <th>備考</th>
                                <td><?php echo get_field('備考'); ?></td>
                            </tr><?php endif; ?>
                        </tbody>
                    </table>

                    <p class="linkbtn1 pt_s pb_l"><a href="<?php bloginfo('url'); ?>/entryform/?entry_type=<?php echo get_field('募集職種'); ?>">応募フォームへ</a></p>
                    <?php else: ?>

                    <p class="pt_s pb_s"><?php echo get_field('募集対象外のメッセージ'); ?></p>
                    <p class="linkbtn1 pt_s pb_l"><a href="<?php bloginfo('url'); ?>/contact/">体験・見学の受付はこちら</a></p>

                    <?php endif; ?>

                </div>
                <!-- wrapper -->
                <div class="obi enter-bottom"></div>
            </section>
            <!-- applicant -->
        </section>
        <!-- page_applicant -->
    </div>
    <!-- contents -->
</div>
<?php get_footer(); ?>
