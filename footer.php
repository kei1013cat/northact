<footer>
    <div class="footer-top cf bg_pink">
        <div class="wrapper">
            <div class="footer_logo"> <a href="#"> <img src="<?php bloginfo('template_url'); ?>/images/footer_logo.svg"> <span class="text">精神科特化型訪問看護ステーション</span> </a> </div>
        </div>
        <!-- wrapper -->
    </div>
    <nav>
        <ul class="cf">
            <li><a href="<?php bloginfo('url'); ?>/">TOP</a></li>
            <li><a href="<?php bloginfo('url'); ?>/visitingcare/">精神科訪問看護</a></li>
            <li><a href="<?php bloginfo('url'); ?>/group/">グループホーム</a></li>
            <!--
            <li><a href="<?php bloginfo('url'); ?>/staff/">ピアスタッフ</a></li>
            <li><a href="<?php bloginfo('url'); ?>/team/">チーム紹介</a></li>
            <li><a href="<?php bloginfo('url'); ?>/partner/">提携医療・福祉機関</a></li>-->
            <li><a href="<?php bloginfo('url'); ?>/company/">会社概要</a></li>
            <li><a href="<?php bloginfo('url'); ?>/recruit/">採用情報</a></li>
        </ul>
    </nav>
    <div class="footer-bottom cf">
        <div class="wrapper">
            <div class="outer cf">
                <div class="footer-contact"><a href="<?php bloginfo('url'); ?>/contact/">お問い合わせ</a></div>
                <div class="access"><a href="<?php bloginfo('url'); ?>/access/">アクセス</a></div>
                <div class="blog"><a href="<?php bloginfo('url'); ?>/blog/">スタッフブログ</a></div>
            </div>
        </div>
        <!-- wrapper -->
    </div>
    <div class="sp" id="bottom-contact">
        <div class="contact-sp"><a href="<?php bloginfo('url'); ?>/contact/"><img src="<?php bloginfo('template_url'); ?>/images/footer_contact_sp.svg"></a></div>
<!--        <div class="facebook-sp"><a href="https://www.facebook.com/%E8%A8%AA%E5%95%8F%E7%9C%8B%E8%AD%B7%E3%82%B9%E3%83%86%E3%83%BC%E3%82%B7%E3%83%A7%E3%83%B3-north-ACT-1387193631587405/" target="_blank"><img src="<?php bloginfo('template_url'); ?>/images/footer_facebook_sp.svg"></a></div>-->
    </div>
    <p class="copy">Copyright &copy;2015-<?php echo date("Y"); ?> northACT CO.,LTD All Rights Reserved.</p>
</footer>
<p id="page_top"><a href="#"><img src="<?php bloginfo('template_url'); ?>/images/pagetop.svg" alt="pagetop"></a></p>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/top.js"></script>
<?php wp_footer(); ?>
</main>
</div>
<!--outer -->
<script src="<?php bloginfo('template_url'); ?>/js/scripts.js"></script>
</body>

</html>
