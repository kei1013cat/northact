<?php get_header(); ?>

<div id="contents_wrap">
    <?php get_template_part('part-title'); ?>
    <?php get_template_part('part-pan'); ?>
    <div id="contents">
        <section id="page_post_detail">
            <section class="post_detail bg_pink pt_s">
                <div class="wrapper wrap-sm">
                    <h2 class="headline2 pt pb_s enter-top"><span class="line"><?php echo get_field('施設名'); ?></span></h2>

                    <div class="outer fead3">
                        <div class="inner cf">
                            <div class="photo">
                                <?php if (has_post_thumbnail()): ?>
                                <img src="<?php the_post_thumbnail_url( 'staff_thum' ); ?>">
                                <?php else: ?>
                                <img src="<?php bloginfo('template_url'); ?>/images/noimage.jpg">
                                <?php endif; ?>

                            </div>
                            <!-- photo -->
                            <div class="text">
                                <h3><span class="cat"><?php echo get_field('施設種別'); ?></span></h3>

                                <dl class="cf">
                                    <dt>名前</dt>
                                    <dd><?php echo get_field('施設名'); ?></dd>
                                </dl>
                                <dl class="cf">
                                    <dt>住所</dt>
                                    <dd><?php echo get_field('住所'); ?></dd>
                                </dl>
                                <dl class="cf">
                                    <dt>TEL</dt>
                                    <dd><?php echo get_field('電話番号'); ?></dd>
                                </dl>
                            </div>
                            <!-- text -->
                        </div>
                        <!-- inner -->

                        <div class="text-box">
                            <?php echo get_field('説明文'); ?>
                        </div>
                    </div>
                    <!-- outer -->

                </div>

            </section>
            <!-- applicant -->
        </section>
        <!-- page_applicant -->
    </div>
    <!-- contents -->
</div>
<?php get_footer(); ?>
