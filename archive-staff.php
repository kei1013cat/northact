<?php get_header(); ?>

<?php
    if ($_GET["area"] != NULL) {
        $area = $_GET["area"];
    } else {
        $area = "all";
    }

    if($area=="all") {
        $area="";
    }
?>

<div id="contents_wrap">
    <?php get_template_part('part-title'); ?>
    <?php get_template_part('part-pan'); ?>
    <div id="contents">

        <section id="page_post">
            <section class="staff">

                <?php
$paged = (int) get_query_var('paged');
$wp_query = new WP_Query();
$param = array(
'post_status' => 'publish',
'paged' => $paged,
'post_type' => 'staff',
'meta_query' => array(
    array(
        'key'=>'札幌市区',
        'value'=>$area,
        'compare'=>'LIKE'
    )
),
'order' => 'DESC'
);
$wp_query->query($param);?>

                <section class="list bg_pink pb_l pt_s">
                    <div class="wrapper wrap-sm pb_l">
                        <h2 class="headline2 pt pb_s enter-top"><span class="line">グループホーム・相談所紹介</span></h2>
                        <section class="top-link pt pb">
                            <ul class="cf enter-bottom">
                                <li class="<?php if($area==''): echo 'hover'; endif; ?>"><a href="<?php bloginfo('url'); ?>/staff/">全て</a></li>
                                <li class="<?php if($area=='chuou'): echo 'hover'; endif; ?>"><a href="<?php bloginfo('url'); ?>/staff/?area=chuou">中央区</a></li>
                                <li class="<?php if($area=='kita'): echo 'hover'; endif; ?>"><a href="<?php bloginfo('url'); ?>/staff/?area=kita">北区</a></li>
                                <li class="<?php if($area=='higashi'): echo 'hover'; endif; ?>"><a href="<?php bloginfo('url'); ?>/staff/?area=higashi">東区</a></li>
                                <li class="<?php if($area=='shiroishi'): echo 'hover'; endif; ?>"><a href="<?php bloginfo('url'); ?>/staff/?area=shiroishi">白石区</a></li>
                                <li class="<?php if($area=='toyohira'): echo 'hover'; endif; ?>"><a href="<?php bloginfo('url'); ?>/staff/?area=toyohira">豊平区</a></li>
                                <li class="<?php if($area=='minami'): echo 'hover'; endif; ?>"><a href="<?php bloginfo('url'); ?>/staff/?area=minami">南区</a></li>
                                <li class="<?php if($area=='nishi'): echo 'hover'; endif; ?>"><a href="<?php bloginfo('url'); ?>/staff/?area=nishi">西区</a></li>
                                <li class="<?php if($area=='atsubetsu'): echo 'hover'; endif; ?>"><a href="<?php bloginfo('url'); ?>/staff/?area=atsubetsu">厚別区</a></li>
                                <li class="<?php if($area=='teine'): echo 'hover'; endif; ?>"><a href="<?php bloginfo('url'); ?>/staff/?area=teine">手稲区</a></li>
                                <li class="<?php if($area=='kiyota'): echo 'hover'; endif; ?>"><a href="<?php bloginfo('url'); ?>/staff/?area=kiyota">清田区</a></li>
                            </ul>
                        </section>
                        <!-- top-link -->

                        <?php if($wp_query->have_posts()):?>
                        <ul class="grid_col2 tab2 sp1 cf">

                            <?php while($wp_query->have_posts()) :?>
                            <?php $wp_query->the_post(); ?>
                            <li>
                                <a href="<?php the_permalink() ?>" title="<?php printf(__('%s', 'kubrick'), the_title_attribute('echo=0')); ?>">
                                    <div class="outer cf">
                                        <div class="photo">
                                            <?php if (has_post_thumbnail()): ?>
                                            <img src="<?php the_post_thumbnail_url( 'staff_thum' ); ?>">
                                            <?php else: ?>
                                            <img src="<?php bloginfo('template_url'); ?>/images/noimage.jpg">
                                            <?php endif; ?>

                                        </div>
                                        <!-- photo -->
                                        <div class="text">
                                            <h3><?php echo get_field('施設名'); ?><span class="cat"><?php echo get_field('施設種別'); ?></span></h3>
                                            <p><?php echo get_field('住所'); ?></p>

                                            <div class="box">
                                                <?php echo mb_substr( get_field('説明文'), 0, 80) . '…'; ?>
                                            </div>
                                        </div>
                                        <!-- text -->
                                        <p class="sp linkbtn">詳しく見る　></p>
                                    </div>

                                </a>
                            </li>
                            <?php endwhile; ?>
                        </ul>

                        <div id="pagination" class="pagination">
                            <?php global $wp_rewrite;
                    $paginate_base = get_pagenum_link(1);
                    if(strpos($paginate_base, '?') || ! $wp_rewrite->using_permalinks()){
                        $paginate_format = '';
                        $paginate_base = add_query_arg('paged','%#%');
                    }
                    else{
                        $paginate_format = (substr($paginate_base,-1,1) == '/' ? '' : '/') .
                        user_trailingslashit('page/%#%/','paged');;
                        $paginate_base .= '%_%';
                    }
                    echo paginate_links(array(
                        'base' => $paginate_base,
                        'format' => $paginate_format,
                        'total' => $wp_query->max_num_pages,
                        'mid_size' => 4,
                        'current' => ($paged ? $paged : 1),
                        'prev_text' => '« 前へ',
                        'next_text' => '次へ »',
                    )); ?>
                        </div><!-- pagination -->
                        <?php endif; ?>
                        <?php wp_reset_query(); ?>

                    </div>
                    <!-- wrapper -->
                </section>
                <!-- seminar -->

                <section class="kouza pb_l">
                    <div class="wrapper wrap-sm">
                        <h2 class="headline2 pt_l pb_s enter-top"><span class="line">養成講座</span></h2>

                        <div class="outer cf">
                            <div class="left">
                                <img src="<?php bloginfo('template_url'); ?>/images/noimage.jpg">
                            </div>
                            <!-- left -->
                            <div class="right">
                                説明が入ります。説明が入ります。説明が入ります。説明が入ります。説明が入ります。説明が入ります。説明が入ります。説明が入ります。説明が入ります。説明が入ります。説明が入ります。説明が入ります。説明が入ります。説明が入ります。説明が入ります。説明が入ります。説明が入ります。説明が入ります。説明が入ります。説明が入ります。説明が入ります。説明が入ります。
                            </div>
                            <!-- right -->
                        </div>
                        <!-- outer -->
                    </div>
                    <!-- wrapper -->
                </section>
                <!-- kouza -->
            </section>
            <!-- visitingcare -->
        </section>
    </div>
    <!-- contents -->

</div>
<?php get_footer(); ?>
