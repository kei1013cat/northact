<section class="recruit">

<?php
$wp_query = new WP_Query();
$param = array(
'posts_per_page' => '-1', //表示件数。-1なら全件表示
'post_status' => 'publish',
'post_type' => 'recruit',
'order' => 'DESC'
);
$wp_query->query($param);?>

<?php if($wp_query->have_posts()):?>

    <section class="job bg_pink pb_l">
        <div class="wrapper wrap-sm">
            <h2 class="headline2 pt_l enter-top"><span class="line">募集職種</span></h2>
            <p class="pb">ノースアクトでは以下の職種を募集しております。<br>お気軽にお問い合わせください。 </p>
              <ul class="grid_col2 tab2 sp1 cf enter-bottom">
                  <?php while($wp_query->have_posts()) :?>
                  <?php $wp_query->the_post(); ?>
                  <li class="col linkbtn <?php echo get_field('ボタンカラー'); ?>">
                  <a href="<?php the_permalink() ?>" title="<?php printf(__('%s', 'kubrick'), the_title_attribute('echo=0')); ?>">
                      <span class="text"><?php echo get_field('ボタン表示名'); ?></span>
                  </a>
                </li>
                <?php endwhile; ?>
              </ul>
          </div>
          <!-- wrapper -->
      </section>
      <!-- seminar -->
  <?php endif; ?>
  <?php wp_reset_query(); ?>
    
    
    <section class="flow pb">
        <div class="wrapper wrap-sm">
            <h2 class="headline2 pt_l enter-top"><span class="line">採用の流れ</span></h2>
            <p class="pb">ノースアクトでは面接の前に体験実習を行っており、<br>仕事内容を把握頂いた上で面接させて頂いております。</p>
            <ol>
                <li class="step1"><dl class="cf">
                    <dt class="cf"><span class="step">STEP1</span><span class="text">応募</span></dt>
                    <dd><span class="text r1">まずはホームページよりご応募ください。</span></dd>
                </dl>
                </li>
                <li class="step2"><dl class="cf">
                    <dt class="cf"><span class="step">STEP2</span><span class="text">体験実習</span></dt>
                    <dd><span class="text r2">ノースアクトの仕事を理解してもらうために<br>実習体験をおこないます。</span></dd>
                </dl>
                </li>
                <li class="step3"><dl class="cf">
                    <dt class="cf"><span class="step">STEP3</span><span class="text">面接</span></dt>
                    <dd><span class="text r2">仕事内容を知っていただいた上で<br>面接させていただきます。</span></dd>
                </dl>
                </li>
                <li class="step4"><dl class="cf">
                    <dt class="cf"><span class="step">STEP4</span><span class="text">採用</span></dt>
                    <dd></dd>
                </dl></li>
            </ol>
            
        </div>
        <!-- wrapper -->
    </section>
    <!-- member -->
</section>
<!-- visitingcare -->