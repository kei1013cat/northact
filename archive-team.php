<?php get_header(); ?>

<?php
// 上部のアンカーリンクのタイトルを取得する
    $paged = (int) get_query_var('paged');
    $wp_query = new WP_Query();
    $param = array(
    'post_status' => 'publish',
    'post_type' => 'team',
    'order' => 'ASC'
    );
    $wp_query->query($param);

    if($wp_query->have_posts()) {
        while($wp_query->have_posts()) {
            $wp_query->the_post();
            $ary_team[] = get_the_title();
        }
    }
    wp_reset_query();
?>


<div id="contents_wrap">
    <?php get_template_part('part-title'); ?>
    <?php get_template_part('part-pan'); ?>
    <div id="contents">

        <section id="page_post">
            <section class="team">

                <?php
$paged = (int) get_query_var('paged');
$wp_query = new WP_Query();
$param = array(
'post_status' => 'publish',
'post_type' => 'team',
'title' => $team,
'order' => 'ASC'
);
$wp_query->query($param);?>

                <section class="list bg_pink pb_l pt_s">
                    <div class="wrapper wrap-sm pb_l">
                        <h2 class="headline2 pt pb_s enter-top"><span class="line">チーム紹介</span></h2>
                        <section class="top-link pt pb">
                            <ul class="cf enter-bottom">
                                <?php $i = 1; ?>
                                <?php foreach($ary_team as $team_val): ?>
                                <li><a href="<?php bloginfo('url'); ?>/team/#a<?php echo str_pad($i,3,0,STR_PAD_LEFT); ?>"><?php echo $team_val; ?></a></li>
                                <?php $i++ ;?>
                                <?php endforeach; ?>
                            </ul>
                        </section>
                        <!-- top-link -->

                        <?php if($wp_query->have_posts()):?>
                        <ul class="grid_col2 tab2 sp1 cf">
                            <?php $i = 1; ?>
                            <?php while($wp_query->have_posts()) :?>
                            <?php $wp_query->the_post(); ?>
                            <li id="a<?php echo str_pad($i,3,0,STR_PAD_LEFT); ?>">
                                <a href="<?php the_permalink() ?>" title="<?php printf(__('%s', 'kubrick'), the_title_attribute('echo=0')); ?>">
                                    <div class="outer cf">
                                        <div class="photo">
                                            <?php if (has_post_thumbnail()): ?>
                                            <img src="<?php the_post_thumbnail_url( 'staff_thum' ); ?>">
                                            <?php else: ?>
                                            <img src="<?php bloginfo('template_url'); ?>/images/noimage.jpg">
                                            <?php endif; ?>

                                        </div>
                                        <!-- photo -->
                                        <div class="text">
                                            <h3><?php the_title(); ?></h3>
                                            <p>メンバー：<?php echo get_field('メンバー名'); ?></p>

                                            <div class="box">
                                                <?php echo mb_substr(wp_strip_all_tags(get_field('説明文')), 0, 80) . '…'; ?>
                                            </div>
                                        </div>
                                        <!-- text -->
                                        <p class="sp linkbtn">詳しく見る　></p>
                                    </div>

                                </a>
                            </li>
                            <?php $i++ ;?>
                            <?php endwhile; ?>
                        </ul>

                        <?php endif; ?>
                        <?php wp_reset_query(); ?>

                    </div>
                    <!-- wrapper -->
                </section>
                <!-- seminar -->

            </section>
            <!-- visitingcare -->
        </section>
    </div>
    <!-- contents -->

</div>
<?php get_footer(); ?>
