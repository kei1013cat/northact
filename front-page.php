<?php get_header(); ?>

<div id="contents_wrap">
    <div id="contents">
        <section class="mainvisual" id="mainvisual">
            <div class="vegas-outer">
                <div style="display:none;" id="vegas-text">
                    <p id="vegas-msg">
                        <div id="text"></div>
                    </p>
                </div>
            </div>
        </section>
        <!-- mainvisual -->

        <section class="top_menu">
            <div class="left-img fead3"><img src="<?php bloginfo('template_url'); ?>/images/index_topmenu_l-img.png"></div>
            <div class="right-img fead3"><img src="<?php bloginfo('template_url'); ?>/images/index_topmenu_r-img.png"></div>
            <div class="wrapper">
                <ul class="cf enter-bottom">
                    <li class="cf">
                        <div class="pc_menu">
                            <img class="grow3" src="<?php bloginfo('template_url'); ?>/images/index_topmenu_icon1.svg">
                            <h3>精神科訪問看護</h3>
                            <p class="linkbtn"><a href="<?php bloginfo('url'); ?>/visitingcare/">詳しく見る</a></p>
                        </div>
                        <a class="sp_menu" href="<?php bloginfo('url'); ?>/visitingcare/">
                            <div class="cf">
                                <div class="photo"><img src="<?php bloginfo('template_url'); ?>/images/index_topmenu_icon1.svg"></div>
                                <h3>精神科訪問看護</h3>
                            </div>
                        </a>
                    </li>
                    <li class="cf">
                        <div class="pc_menu">
                            <img class="grow3" src="<?php bloginfo('template_url'); ?>/images/index_topmenu_icon2.svg">
                            <h3>グループホーム</h3>
                            <p class="linkbtn"><a href="<?php bloginfo('url'); ?>/group/">詳しく見る</a></p>
                        </div>
                        <a class="sp_menu" href="<?php bloginfo('url'); ?>/group/">
                            <div class="cf">
                                <div class="photo"><img src="<?php bloginfo('template_url'); ?>/images/index_topmenu_icon2.svg"></div>
                                <h3>グループホーム</h3>
                            </div>
                        </a>
                    </li>
                    <li class="cf">
                        <div class="pc_menu">
                            <img class="grow3" src="<?php bloginfo('template_url'); ?>/images/index_topmenu_icon3.svg">
                            <h3>採用情報</h3>
                            <p class="linkbtn"><a href="<?php bloginfo('url'); ?>/recruit/">詳しく見る</a></p>
                        </div>
                        <a class="sp_menu" href="<?php bloginfo('url'); ?>/recruit/">
                            <div class="cf">
                                <div class="photo"><img src="<?php bloginfo('template_url'); ?>/images/index_topmenu_icon3.svg"></div>
                                <h3>採用情報</h3>
                            </div>
                        </a>
                    </li>
                </ul>
            </div>
        </section>
        <!-- top_menu -->

        <section class="about">
            <div class="left-img enter-left"><img src="<?php bloginfo('template_url'); ?>/images/index_about_l-img.jpg"></div>
            <div class="right-img enter-right"><img src="<?php bloginfo('template_url'); ?>/images/index_about_r-img.jpg"></div>
            <div class="wrapper">
                <p class="enter-bottom1">私たちはACTプログラムの理念・要素を取り入れた訪問看護を展開し<br class="pc">
                    重い精神障害を抱えた方への身体的サポート・精神的サポート・<br class="pc">
                    社会的サポート・福祉的サポート・家族のサポート・地域へのサポートを実施します。</p>
            </div>
        </section>
        <!-- about -->


        <?php
        $wp_query = new WP_Query();
        $param = array(
        'posts_per_page' => '5', //表示件数。-1なら全件表示
        'post_status' => 'publish',
        'orderby' => 'date', //ID順に並び替え
        'order' => 'DESC'
        );
        $wp_query->query($param);?>
        <?php if($wp_query->have_posts()):?>
        <section class="news bg_pink pt">
            <section class="news pb_l">
                <div class="wrapper">
                    <h3 class="headline1 pink pb_s enter-top"><span class="text">お知らせ</span></h3>
                    <div class="outer enter-bottom">
                        <?php while($wp_query->have_posts()) :?>
                        <?php $wp_query->the_post(); ?>
                        <dl class="cf news_list">
                            <dt>
                                <?php
                                $category = get_the_category();
                                if(!empty($category)):?>
                                <span class="cat orange"><?php echo $category[0]->cat_name; ?></span>
                                <?php endif; ?>

                            </dt>
                            <dd>
                                <a href="<?php the_permalink() ?>" rel="bookmark" title="<?php printf(__('%s', 'kubrick'), the_title_attribute('echo=0')); ?>">
                                    <?php the_time('Y年m月d日'); ?>　<?php echo $post->post_title; ?></a>
                            </dd>
                        </dl>
                        <?php endwhile; ?>
                        <!-- inner -->
                        <div class="link-list">
                            <p class="list"><a href="<?php bloginfo('url'); ?>/newslist/">一覧へ</a></p>
                        </div>
                    </div>
                    <!-- outer -->
                </div>
            </section>
            <!-- news -->
        </section>
        <!-- news -->
        <?php endif; ?>




        <?php
        $wp_query = new WP_Query();
        $param = array(
        'posts_per_page' => '10', //表示件数。-1なら全件表示
        'post_status' => 'publish',
        'orderby' => 'date', //ID順に並び替え
        'post_type' => 'blog',
        'order' => 'DESC'
        );
        $wp_query->query($param);?>
        <?php if($wp_query->have_posts()):?>
        <section class="news bg_green pt">
            <section class="news pb_l">
                <div class="wrapper">
                    <h3 class="headline1 green pb_s enter-top"><span class="text">ブログ</span></h3>
                    <div class="outer enter-bottom">
                        <?php while($wp_query->have_posts()) :?>
                        <?php $wp_query->the_post(); ?>
                        <dl class="cf news_list">
                            <dt>
                                <?php
                                $category = get_the_terms($post_id, 'blog-cat');
                                if(!empty($category)):?>
                                <span class="cat green"><?php echo $category[0]->name; ?></span>
                                <?php endif; ?>
                            </dt>
                            <dd>
                                <a href="<?php the_permalink() ?>" rel="bookmark" title="<?php printf(__('%s', 'kubrick'), the_title_attribute('echo=0')); ?>">
                                    <?php the_time('Y年m月d日'); ?>　<?php echo $post->post_title; ?></a>
                            </dd>
                        </dl>
                        <?php endwhile; ?>
                        <!-- inner -->
                        <div class="link-list">
                            <p class="list"><a href="<?php bloginfo('url'); ?>/blog/">一覧へ</a></p>
                        </div>
                    </div>
                    <!-- outer -->
                </div>
            </section>
            <!-- news -->
        </section>
        <!-- news -->
        <?php endif; ?>





        <?php
        $wp_query = new WP_Query();
        $param = array(
        'posts_per_page' => '8', //表示件数。-1なら全件表示
        'post_status' => 'publish',
        'orderby' => 'date', //ID順に並び替え
        'post_type' => 'newsletter',
        'order' => 'DESC'
        );
        $wp_query->query($param);?>
        <?php if($wp_query->have_posts()):?>
        <section class="newsletter bg_pink pt_l">
            <section class="news pb_l">
                <div class="wrapper">
                    <h3 class="headline1 pink pb enter-top"><span class="text">ニュースレター</span></h3>
                    <ul class="pdf_list cf enter-bottom">
                        <?php while($wp_query->have_posts()) :?>
                        <?php $wp_query->the_post(); ?>
                        <li>
                            <a href="<?php echo get_field('pdf'); ?>" target="_blank" rel="bookmark" title="<?php printf(__('%s', 'kubrick'), the_title_attribute('echo=0')); ?>">
                                <?php if (has_post_thumbnail()): ?>
                                <img src="<?php the_post_thumbnail_url( 'pdf_thum' ); ?>">
                                <?php else: ?>
                                <img src="<?php bloginfo('template_url'); ?>/images/newsletter_img.jpg">
                                <?php endif; ?>
                            </a>
                            <p><?php echo $post->post_title; ?></p>
                        </li>
                        <?php endwhile; ?>
                    </ul>
                    <!-- inner -->
                    <div class="bgnumber pt">
                        <p class="list"><a href="<?php bloginfo('url'); ?>/newsletter/">バックナンバーを見る</a></p>
                    </div>
                </div>
            </section>
            <!-- news -->
        </section>
        <!-- newsletter -->
        <?php endif; ?>




    </div>
    <!--contents -->
</div>
<!--contents_wrap -->
<?php get_footer(); ?>
