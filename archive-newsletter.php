<?php get_header(); ?>


<?php
$paged = (int) get_query_var('paged');
$wp_query = new WP_Query();
$param = array(
'post_status' => 'publish',
'paged' => $paged,
'post_type' => 'newsletter',
'order' => 'DESC'
);
$wp_query->query($param);?>


<div id="contents">
	<?php include (TEMPLATEPATH . '/part-title.php'); ?>
	<section class="blog bg_pink">
		<div class="wrapper cf">
			
        <h2 class="headline2 pt_l pb enter-top"><span class="line">ニュースレター記事一覧</span></h2>
        <?php if($wp_query->have_posts()):?>
        <section class="news">
        <ul class="pdf_list cf">
      <?php while($wp_query->have_posts()) :?>
      <?php $wp_query->the_post(); ?>

                <li>
                    <a href="<?php echo get_field('pdf'); ?>" target="_blank" rel="bookmark" title="<?php printf(__('%s', 'kubrick'), the_title_attribute('echo=0')); ?>">
                        <?php if (has_post_thumbnail()): ?>
                          <img src="<?php the_post_thumbnail_url( 'pdf_thum' ); ?>">
                        <?php else: ?>
                          <img src="<?php bloginfo('template_url'); ?>/images/newsletter_img.jpg" >
                        <?php endif; ?>                        
                    </a>
                    <p><?php echo $post->post_title; ?></p>
                </li>


				<?php endwhile; ?>
                </ul>
				</section>
                
                <div id="pagination" class="pagination">
                    <?php global $wp_rewrite;
                    $paginate_base = get_pagenum_link(1);
                    if(strpos($paginate_base, '?') || ! $wp_rewrite->using_permalinks()){
                        $paginate_format = '';
                        $paginate_base = add_query_arg('paged','%#%');
                    }
                    else{
                        $paginate_format = (substr($paginate_base,-1,1) == '/' ? '' : '/') .
                        user_trailingslashit('page/%#%/','paged');;
                        $paginate_base .= '%_%';
                    }
                    echo paginate_links(array(
                        'base' => $paginate_base,
                        'format' => $paginate_format,
                        'total' => $wp_query->max_num_pages,
                        'mid_size' => 4,
                        'current' => ($paged ? $paged : 1),
                        'prev_text' => '« 前へ',
                        'next_text' => '次へ »',
                    )); ?>
                </div><!-- pagination -->
          <?php endif; ?>
          <?php wp_reset_query(); ?>

		</div>
		<!-- wrapper -->
	</section>
</div>
<!-- contents -->

<?php get_footer(); ?>
