<section class="group">
    <section class="top bg_pink">
        <div class="wrapper wrap-sm">
            <h2 class="headline2 pt_l"><span class="line">フィット麻生・パステル麻生</span></h2>
            <ul class="list pt pb_l">
                <li>精神障がい者への共同生活を援助します</li>
                <li>現在定員14名</li>
                <li>プライベート空間を大切にしています。</li>
                <li>管理者1名、サービス管理者1名、世話人7名（ピアサポーター）</li>
                <li>季節や入居者様の希望に応じて、定期的にイベントを開催しております。</li>
                <li>north-ACT事務所より10分圏内、訪問看護も同時に利用できます。</li>
            </ul>
        </div>
        <!-- wrapper -->
        <div class="bg_photo fead"></div>
    </section>
    <!-- top -->
    
    <section class="access bg_pink">
        <h2 class="headline2 pt_l pb_s"><span class="line">所在地</span></h2>

        <div class="grid_ptn1 type1 cf enter-left">
            <div class="photo col">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d182.0612283264697!2d141.34425878712145!3d43.104942488204344!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x5f0b28c62c492be9%3A0x8ab51ec7d3a612c7!2z44CSMDAxLTAwMzcg5YyX5rW36YGT5pyt5bmM5biC5YyX5Yy65YyX77yT77yX5p2h6KW_77yS5LiB55uu77yS4oiS77yR77yWIOm6u-eUn--8s-ODu--8qw!5e0!3m2!1sja!2sjp!4v1553012214032" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
            <div class="text col">
                <div class="outer">
                    <h3 class="pb_s">グループホームフィット麻生</h3>
                    <p class="pb_s">札幌市北区北37条西2丁目2-16<br>麻生SK102号室</p>
                    <p>地下鉄南北線『麻生駅』徒歩7分</p>
                </div>
            </div>
            <!-- text -->
        </div>
        <!-- grid-ptn1 -->

        <div class="grid_ptn1 type2 cf enter-right">
            <div class="photo col">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d182.0612283264697!2d141.34425878712145!3d43.104942488204344!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x5f0b28c62c492be9%3A0x8ab51ec7d3a612c7!2z44CSMDAxLTAwMzcg5YyX5rW36YGT5pyt5bmM5biC5YyX5Yy65YyX77yT77yX5p2h6KW_77yS5LiB55uu77yS4oiS77yR77yWIOm6u-eUn--8s-ODu--8qw!5e0!3m2!1sja!2sjp!4v1553012214032" frameborder="0" style="border:0" allowfullscreen></iframe>

            </div>
            <div class="text col">
                <div class="outer">
                    <h3 class="pb_s">グループホームパステル麻生</h3>
                    <p class="pb_s">札幌市北区北37条西2丁目2-15<br>ハニサクル麻生101号室</p>
                    <p>地下鉄南北線『麻生駅』徒歩7分</p>
                </div>
            </div>
            <!-- text -->
        </div>
        <!-- grid-ptn1 -->

    </section>
    <!-- access -->

</section>
<!-- visitingcare -->