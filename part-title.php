<?php
$url = $_SERVER['REQUEST_URI'];
$parent_id = $post->post_parent; // 親ページのIDを取得
$parent_slug = get_post($parent_id)->post_name; // 親ページのスラッグを取得

?>
<?php
$parent_slug = '';
if($post->post_parent){
  $parent_slug = get_page_uri($post->post_parent).'_';
}
$imagesrc = 'pagetitle_'.$parent_slug.$post->post_name.'.jpg';

?>

<?php if(is_singular('recruit') || is_post_type_archive("recruit")):?>

<div id="pagetitle-photo" class="fead4" style="background:url(<?php bloginfo('template_url');?>/images/pagetitle_recruit.jpg) no-repeat center center; background-size:cover;">
    <div class="title-outer enter-title">
        採用情報
    </div>
</div>

<?php elseif(is_singular('staff') || is_post_type_archive("staff")):?>

<div id="pagetitle-photo" class="fead4" style="background:url(<?php bloginfo('template_url');?>/images/pagetitle_staff.jpg) no-repeat center center; background-size:cover;">
    <div class="title-outer enter-title">
        ピアスタッフ
    </div>
</div>

<?php elseif(is_singular('team') || is_post_type_archive("team")):?>

<div id="pagetitle-photo" class="fead4" style="background:url(<?php bloginfo('template_url');?>/images/pagetitle_team.jpg) no-repeat center center; background-size:cover;">
    <div class="title-outer enter-title">
        チーム紹介
    </div>
</div>

<?php elseif(is_post_type_archive("partner")):?>

<div id="pagetitle-photo" class="fead4" style="background:url(<?php bloginfo('template_url');?>/images/pagetitle_partner.jpg) no-repeat center center; background-size:cover;">
    <div class="title-outer enter-title">
        提携医療・福祉機関
    </div>
</div>

<?php elseif(is_singular('blog') || is_post_type_archive("blog")):?>

<div id="pagetitle-photo" class="fead4" style="background:url(<?php bloginfo('template_url');?>/images/pagetitle_blog.jpg) no-repeat center center; background-size:cover;">
    <div class="title-outer enter-title">
        スタッフブログ
    </div>
</div>

<?php elseif(is_post_type_archive("newsletter")):?>

<div id="pagetitle-photo" class="fead4" style="background:url(<?php bloginfo('template_url');?>/images/pagetitle_staff.jpg) no-repeat center center; background-size:cover;">
    <div class="title-outer enter-title">
        ニュースレター
    </div>
</div>


<?php elseif(is_single() || $post->post_name=="newslist"):?>

<div id="pagetitle-photo" class="fead4" style="background:url(<?php bloginfo('template_url');?>/images/pagetitle_blog.jpg) no-repeat center center; background-size:cover;">
    <div class="title-outer enter-title">
        お知らせ
    </div>
</div>


<?php elseif($post->post_name=="entryform"):?>

<div id="pagetitle-photo" class="fead4" style="background:url(<?php bloginfo('template_url');?>/images/pagetitle_recruit.jpg) no-repeat center center; background-size:cover;">
    <div class="title-outer enter-title">
        応募フォーム
    </div>
</div>


<?php else: ?>


<div id="pagetitle-photo" class="fead4" style="background:url(<?php bloginfo('template_url');?>/images/<?php echo $imagesrc; /*$image_tmp = get_field('タイトル画像');if( !empty($image_tmp) ){ echo $image_tmp['sizes']['pagetitle'];}*/?>) no-repeat center center; background-size:cover;">
    <div class="title-outer enter-title">
        <?php echo get_post($post->post_parent)->post_title;?>
    </div>
</div>
<?php endif; ?>
